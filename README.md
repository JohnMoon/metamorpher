# Metamorpher

Metamorpher morphs your data into metadata! Basically, it takes an input file
and base64-encodes it into a directory structure whose *filenames* include the
data. Why is this useful? I don't know! This is really a solution looking for
a problem... let me know if you think of anything!

## Usage
```
USAGE:
    metamorpher [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --decode <DIR>         Decode a directory tree into a file
    -e, --encode <FILE>        Encode a file into a directory tree
    -o, --output <DIR/FILE>    When encoding, output the encoded directory to DIR. When decoding, output the decoded
                               file to FILE
```

## Example
First let's generate 100 megabytes of random data to work with:
```bash
$ dd if=/dev/urandom of=./random_data bs=1M count=100
```
### Encoding
Pass a file with the `-e` flag to encode:
```bash
$ metamorpher -e ./random_data
```

The encoded directory is created in the working directory. It's called `<filename>_metamorphed`.
You can also pass a custom directory name with the `-o` flag.

Here are three entries from the directory tree we just created:
```bash
$ find ./random_data_metamorphed -mindepth 2 | tail -n 3
./random_data_metamorphed/8913d.HWdVeDtcf5Q8RDSy9iAK7BaOSIMi2dG5W9IZfhkLUX_qlwSjIWe1tJ_uFfpI-R7LRsGqurrviiPruE-sxP-s3gFQGJHbhuV5z1L2iTZOXsdOiVk_Q_lzkO0jx7boZ5Dm1NZmJtUAT8F7YVyherNkru7d2CRaxULF7Uo9UnbMvZfR16MGV4SzKX-SScIIRUARL6q8tubr2N-BIwRp6nfr57xajTkPlIv4to-x9CXCF765Zs3OyGY_eMijH
./random_data_metamorphed/89145.cY1wcToYWMW8oYj_C9ttE22iGbt6z3FDcgVLvfOMFgFG2rBtsN0rpJb5ZEO3sb_72jWbeWJ8CuFBomoiBrSu2tB4_ITa9rWZhcIB62sERQQwARqdFE2PbwMh0Uuuf1-y2wkazgJdYE0o5jO4NkxS7jj1C-b5--OY538ibwZizbG3IS-c0-1nFyhNRMsU1mgcuWp8BpavmYZoDJ_KmZDOJjUV0ZAPxrtuuGmfRdg5ZIzx5mmB-mztIoqCM
./random_data_metamorphed/655fe9206ffff5d7fbc2bbe426f0b2330b4eb567.sha1
```

The first two entries are a couple of the thousands of files that were created with the random
data encoded into their filenames. The last entry has a SHA1 checksum of the original file in
its filename. This allows us to verify integrity after decoding.

For this example, on an EXT4 filesystem, the encoded directory takes up about 209MB on disk,
so there's a rough doubling of the data size when using this method. EXT4 allows a maximum
filename length of 255 characters, and the hexadecimal ordering characters at the beginning
of each filename (plus a delimiter) subtract 6 characters from that number. This leaves 249
characters per filename for the encoded data.

The 100MB of data, encoded in base64, is 139,810,136 characters long. 139,810,136 / 249 =
561487 (rounded up to nearest integer).

Sure enough, we see that many files (plus one checksum file):
```bash
$ find ./random_data_metamorphed -type f | wc -l
561488
```

Every file in the directory is zero-length. All disk usage is from the filesystem overhead!

On my laptop, the encoding process for this example took about 18 seconds.

### Decoding
Just pass the encoded directory back to the tool with the `-d` flag to decode it:
```bash
$ metamorpher -d ./random_data_metamorphed -o ./copy_of_random_data
```

This step will automatically verify the checksum created during encoding. If no output is
file is passed with the `-o` flag, the tool outputs the decoded data to stdout.

The decoding process for this example took about 3 seconds on my laptop.

## OS/Filesystem Support
I've tested this tool on Arch Linux (ext4) and Windows 10 (NTFS).
Portability was a major design goal, so please let me know if you've
gotten it to work (or ran into trouble) on other systems!

Since the tool does rely heavily on the maximum filename length allowed by
the filesystem, this parameter is detected at runtime by attempting to create
files with shorter and shorter filenames until a file creation is allowed. This method
may not be the most efficient, but it's OS and FS agnostic which is a big win.

## Building

Using the Cargo build system, simply clone this repo and run `cargo build --release`
inside of it! The binary will be generated in `./target/release/metamorpher`.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
