#[macro_use]
extern crate clap;
extern crate base64;
extern crate crypto;

use clap::App;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use self::base64::{engine::general_purpose::URL_SAFE, Engine as _};
use self::crypto::digest::Digest;
use self::crypto::sha1::Sha1;

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    let out = matches.value_of("output");

    if let Some(f) = matches.value_of("encode") {
        if let Err(e) = encode(f, out) {
            eprintln!("Failed to encode file: {}", e);
        };
    };

    if let Some(d) = matches.value_of("decode") {
        if let Err(e) = decode(d, out) {
            eprintln!("Failed to decode directory: {}", e);
        };
    };
}

/// Encodes a given file into a directory structure of base64 file names
fn encode(f: &str, output: Option<&str>) -> std::io::Result<()> {
    let file_path = Path::new(&f);

    // We can only operate on files
    if file_path.is_dir() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "cannot encode directory, please pass a file",
        ));
    }

    println!("Encoding {}... ", f);

    // If no output dirname given, use {fname}_metamorphed
    let new_dir = match output {
        Some(output) => output.to_string(),
        None => {
            let fname = file_path
                .file_name()
                .expect("failed to determine file name")
                .to_str()
                .expect("failed to convert file name to UTF8 string");

            format!("{}_metamorphed", &fname)
        }
    };

    // Read all the data into a buffer
    let mut file = File::open(f)?;
    let mut data = Vec::new();
    file.read_to_end(&mut data)?;

    // Encode the data into a base64 string
    let b64 = URL_SAFE.encode(&data);
    println!("Base64 length: {}", b64.len());
    let (max_fname_len, pad_size) = calc_max_fname_len(&new_dir, b64.len());

    // Chunk the string into max_fname_len bits and create a file named after
    // the chunk
    std::fs::create_dir_all(&new_dir)?;
    for (sub_dir, block) in b64.as_bytes().chunks(max_fname_len).enumerate() {
        let b64_chunk =
            std::str::from_utf8(block).expect("failed to convert base64 chunk to UTF8 string");
        let b64_chunk_prefixed = format!("{:01$x}.{2}", sub_dir, pad_size, b64_chunk);
        let chunk_path = Path::new(&new_dir).join(b64_chunk_prefixed);
        std::fs::File::create(chunk_path)?;
    }

    // Generate a hash file
    gen_hash(&data, &new_dir)?;
    println!(
        "Sucessfully encoded! Output and checksum in \"{}\".",
        new_dir
    );

    Ok(())
}

/// Decodes a given directory and outputs data to optional output
fn decode(d: &str, output: Option<&str>) -> std::io::Result<()> {
    let dir_path = Path::new(&d);
    if !dir_path.is_dir() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "cannot decode a file, please pass a directory",
        ));
    }

    // Index the directory and sort it
    let mut entries: Vec<_> = std::fs::read_dir(d)
        .expect("failed to index encoded directory")
        .map(std::result::Result::unwrap)
        .collect();
    entries.sort_by_key(std::fs::DirEntry::file_name);

    let mut sha1sum = "".to_string();
    let mut base64_str = "".to_string();

    // For each file, append the filename to base64_str
    for entry in entries {
        let path = entry.path();
        if let Some(ext) = path.extension() {
            // If it's the checksum file, parse it and continue
            if ext == "sha1" {
                if let Some(stem) = path.file_stem() {
                    sha1sum = stem
                        .to_str()
                        .expect("could not parse checksum from filename")
                        .to_string();
                    continue;
                }
            // Else, the "extension" is the base64 data
            } else {
                base64_str += ext.to_str().unwrap();
            }
        }
    }

    // Decode the data
    let decoded_data = URL_SAFE.decode(&base64_str).expect("failed to decode data");

    // Write data to the output
    let out_name = match output {
        Some(output) => {
            let mut out = std::fs::File::create(output)?;
            out.write_all(&decoded_data)
                .unwrap_or_else(|_| panic!("failed to write decoded data to {}", output));
            format!("\"{}\"", output)
        }
        None => {
            let out = std::io::stdout();
            let mut handle = out.lock();
            handle
                .write_all(&decoded_data)
                .expect("failed to write decoded data to stdout");
            "stdout".to_string()
        }
    };

    // Check the hash
    if hash_good(&decoded_data, &sha1sum) {
        eprintln!(
            "Checksum verified! {} bytes written to {}.",
            decoded_data.len(),
            out_name
        );
    } else {
        eprintln!(
            "Checksum failed! {} bytes written to {}, but integrity cannot be guaranteed.",
            decoded_data.len(),
            out_name
        );
    }

    Ok(())
}

/// Generate a sha1sum of the file and place it in a new file
/// called <sha1sum>.sha1
fn gen_hash(data: &[u8], new_dir: &str) -> std::io::Result<()> {
    let mut hasher = Sha1::new();
    hasher.input(data);

    let hash_file_path = Path::new(&new_dir).join(format!("{}.sha1", hasher.result_str()));
    File::create(hash_file_path)?;
    Ok(())
}

/// Check if the hash is good
fn hash_good(data: &[u8], sha1sum: &str) -> bool {
    let mut hasher = Sha1::new();
    hasher.input(data);

    if sha1sum.is_empty() {
        eprintln!("No hash file found...");
    }

    hasher.result_str() == sha1sum
}

/// Test to figure out how long a filename can be
fn calc_max_fname_len(dir: &str, b64_len: usize) -> (usize, usize) {
    let test_dir = format!(".{}", &dir);
    let test_dir_path = Path::new(&test_dir);
    std::fs::create_dir_all(test_dir_path)
        .expect("failed to create filename length test directory");

    // Start at some stupidly high value and work backward
    let mut f_len = 1024;
    loop {
        let test = format!("{:X<0$}", f_len);
        let path = test_dir_path.join(&test);
        if std::fs::File::create(&path).is_err() {
            f_len -= 1;
        } else {
            if let Err(e) = std::fs::remove_dir_all(test_dir_path) {
                eprintln!(
                    "Warning: unable to remove test directory \"{}\" - {}",
                    &test_dir, e
                );
            }
            break;
        }
    }

    // Pad size is how many chars are needed to pad the length in hex
    let pad_size = format!("{:x}", b64_len / f_len).chars().count();

    // Eventual format will be <dir>/<padded ordering>.<b64 encoded data>,
    // so subtract the pad_size and the delimiting dot from the f_len
    (f_len - pad_size - 1, pad_size)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_encode_and_decode() {
        let exe_path = std::env::current_exe().unwrap();
        let morphed_path = format!("{}_metamorphed", exe_path.to_str().unwrap());

        // Encode the metamorpher binary itself
        if let Err(e) = encode(exe_path.to_str().unwrap(), Some(&morphed_path)) {
            eprintln!("{}", e);
            panic!("Failed to encode file");
        }

        let mut decode_pass = true;

        // Decode the binary
        // TODO: add windows path
        if let Err(e) = decode(&morphed_path, Some("/dev/null")) {
            eprintln!("{}", e);
            decode_pass = false;
        }

        std::fs::remove_dir_all(&morphed_path).unwrap();
        assert!(decode_pass)
    }

    #[test]
    fn bail_on_non_existant_file() {
        assert!(encode("asdf.qwerty", None).is_err());
    }

    #[test]
    fn bail_on_directory_encode() {
        let pwd = std::env::current_dir().unwrap();
        assert!(encode(pwd.to_str().unwrap(), None).is_err());
    }

    #[test]
    fn bail_on_file_decode() {
        let exe_path = std::env::current_exe().unwrap();
        assert!(decode(exe_path.to_str().unwrap(), None).is_err());
    }
}
